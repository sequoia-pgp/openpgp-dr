//! Double Ratchet using Diffie-Hellman over Cv25519, HKDF with
//! SHA256, and HMAC with SHA256.
//!
//! This module instanciates the Double Ratchet algorithm following
//! the recommendation of Section 5.2 of the [Double Ratchet Algorithm
//! Spec].
//!
//!   [Double Ratchet Algorithm Spec]: https://signal.org/docs/specifications/doubleratchet/

use anyhow::Result;

use nettle::{
    curve25519::*,
    hash::Sha256,
    kdf::hkdf,
    mac::*,
    random::*,
};

use crate::{
    Key,
};

/// A pair of Diffie-Hellman parameters over Cv25519.
pub type PreKeyPair = crate::PreKeyPair::<DH>;
/// Fully initialized Double Ratchet using Diffie-Hellman over
/// Cv25519, HKDF with SHA256, and HMAC with SHA256.
pub type Ratchet = crate::Ratchet::<DH, KdfRK, KdfCK>;
/// Half-initialized Double Ratchet using Diffie-Hellman over Cv25519,
/// HKDF with SHA256, and HMAC with SHA256.
pub type RatchetHalfOpen = crate::RatchetHalfOpen::<DH, KdfRK, KdfCK>;

/// Diffie-Hellman over Cv25519.
pub struct DH {}

impl crate::DH for DH {
    type Secret = [u8; CURVE25519_SIZE];
    type Public = [u8; CURVE25519_SIZE];
    fn generate() -> Result<(Self::Secret, Self::Public)> {
        let mut rng = Yarrow::default();
        let s = private_key(&mut rng);

        let mut secret = Self::Secret::default();
        &secret[..].copy_from_slice(&s);

        let mut public = Self::Public::default();
        mul_g(&mut public, &secret)?;

        Ok((secret, public))
    }

    fn derive(secret: &Self::Secret, public: &Self::Public)
              -> Result<Self::Secret> {
        let mut shared = Self::Secret::default();
        mul(&mut shared, secret, public)?;
        Ok(shared)
    }
}

/// Info input for the HKDF algorithm.
pub const HKDF_INFO: &[u8] = b"Double Ratchet for OpenPGP";

/// HKDF instantiated with SHA256.
pub struct KdfRK(Key);

impl crate::KdfRK for KdfRK {
    fn new(k: Key) -> Self {
        KdfRK(k)
    }

    fn step(&mut self, key_in: &[u8]) -> Result<Key> {
        let mut key = [0u8; 64];
        hkdf::<Sha256>(key_in,  // HKDF key
                       &self.0, // HKDF salt
                       HKDF_INFO, // HKDF info
                       &mut key); // HKDF result
        self.0.copy_from_slice(&key[..32]);
        let mut out = Key::default();
        out.copy_from_slice(&key[32..]);
        Ok(out)
    }
}

/// HMAC instantiated with SHA256.
pub struct KdfCK(Key);

impl crate::KdfCK for KdfCK {
    fn new(k: Key) -> Self {
        KdfCK(k)
    }

    fn step(&mut self) -> Result<Key> {
        use crate::CKInput::*;

        // Generate message key.
        let mut h = Hmac::<Sha256>::with_key(&self.0);
        h.update(MessageKey.as_bytes());
        let mut mk = Key::default();
        h.digest(&mut mk)?;

        // Generate next chain key.
        let mut h = Hmac::<Sha256>::with_key(&self.0);
        h.update(ChainKey.as_bytes());
        h.digest(&mut self.0)?;

        Ok(mk)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use sequoia_openpgp::fmt::hex;

    // These are the test vectors from RFC7748.
    #[test]
    fn rfc7748() -> Result<()> {
        let alice_sec = hex::decode(
            "77076d0a7318a57d3c16c17251b26645df4c2f87ebc0992ab177fba51db92c2a"
        )?;
        let alice_pub = hex::decode(
            "8520f0098930a754748b7ddcb43ef75a0dbf3a0d26381af4eba4a98eaa9b4e6a"
        )?;

        let mut alice_pub_derived = vec![0; CURVE25519_SIZE];
        mul_g(&mut alice_pub_derived, &alice_sec)?;
        assert_eq!(alice_pub_derived, alice_pub);

        let bob_sec = hex::decode(
            "5dab087e624a8a4b79e17f8b83800ee66f3bb1292618b6fd1c2f8b27ff88e0eb"
        )?;
        let bob_pub = hex::decode(
            "de9edb7d7b7dc1b4d35b61c2ece435373f8343c85b78674dadfc7e146f882b4f"
        )?;

        let mut bob_pub_derived = vec![0; CURVE25519_SIZE];
        mul_g(&mut bob_pub_derived, &bob_sec)?;
        assert_eq!(bob_pub_derived, bob_pub);

        let shared_secret = hex::decode(
            "4a5d9d5ba4ce2de1728e3bf480350f25e07e21c947d19e3376f09b3c1e161742"
        )?;

        let mut alices_shared_secret = vec![0; CURVE25519_SIZE];
        mul(&mut alices_shared_secret, &alice_sec, &bob_pub)?;
        assert_eq!(&shared_secret, &alices_shared_secret);

        let mut bobs_shared_secret = vec![0; CURVE25519_SIZE];
        mul(&mut bobs_shared_secret, &bob_sec, &alice_pub)?;
        assert_eq!(&shared_secret, &bobs_shared_secret);

        Ok(())
    }

    #[test]
    fn basic() -> Result<()> {
        let bob_prekey_pair = PreKeyPair::new()?;
        let bob_prekey = bob_prekey_pair.pre_key();

        let mut alice = RatchetHalfOpen::new(bob_prekey)?;
        let (h, mk) = alice.encrypt()?;
        eprintln!("Header {{ dh: {}, pn: {}, n_s: {} }}",
                  hex::encode_pretty(&h.dh_send), h.pn, h.n_s);
        eprintln!("{}", hex::encode_pretty(&mk));

        let (mut bob, mk_) = Ratchet::new(bob_prekey_pair, h)?;
        eprintln!("{}", hex::encode_pretty(&mk));
        assert_eq!(mk, mk_);

        // Have alice send another message before Bob replied.
        let (h, mk) = alice.encrypt()?;
        let mk_ = bob.decrypt(h)?;
        assert_eq!(mk, mk_);

        // Bob replies.
        let (h, mk) = bob.encrypt()?;
        let (mut alice, mk_) = alice.decrypt(h)?;
        assert_eq!(mk, mk_);

        // All ratchets are fully initialized at this point.  Alice
        // replies.
        let (h, mk) = alice.encrypt()?;
        let mk_ = bob.decrypt(h)?;
        assert_eq!(mk, mk_);

        // Check out-of-order decryption.
        let (h0, mk0) = bob.encrypt()?;
        let (h1, mk1) = bob.encrypt()?;

        let mk1_ = alice.decrypt(h1)?;
        assert_eq!(mk1, mk1_);
        let mk0_ = alice.decrypt(h0)?;
        assert_eq!(mk0, mk0_);

        Ok(())
    }
}
