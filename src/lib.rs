//! An implementation of Signal's Double-Ratchet protocol for use in
//! OpenPGP.
//!
//! This algorithm has been popularized by Signal.  It uses a series
//! of Cryptographic Ratchets to derive session keys.  See the [Double
//! Ratchet Algorithm Spec] for a full description.
//!
//! This implementation is geared towards use in OpenPGP.  It does not
//! encrypt messages, it merely derives the session keys.
//!
//!   [Double Ratchet Algorithm Spec]: https://signal.org/docs/specifications/doubleratchet/
//!
//! # Examples
//!
//! This example demonstrates how to generate a pre-key and initialize
//! two Double Ratchets.
//!
//! ```
//! # fn main() -> anyhow::Result<()> {
//! use openpgp_dr::cv25519::{PreKeyPair, RatchetHalfOpen, Ratchet};
//!
//! // Generate a pre-key for Bob.
//! let bob_prekey_pair = PreKeyPair::new()?;
//!
//! // The public half of that pair is communicated to Alice.
//! let bob_prekey = bob_prekey_pair.pre_key();
//!
//! // Now, Alice can start initializing her Ratchets.
//! let mut alice = RatchetHalfOpen::new(bob_prekey)?;
//!
//! // And she can immediately start encrypting messages.
//! let (header, mk) = alice.encrypt()?;
//!
//! // Now, Bob can initialize his Ratchets, and in the process
//! // decrypt the first message.
//! let (mut bob, mk_) = Ratchet::new(bob_prekey_pair, header)?;
//! assert_eq!(mk, mk_);
//!
//! // Bob replies.
//! let (header, mk) = bob.encrypt()?;
//!
//! // And Alice decrypts, finalizing her initialization in the process.
//! let (alice, mk_) = alice.decrypt(header)?;
//! assert_eq!(mk, mk_);
//!
//! // All ratchets are fully initialized at this point.
//! # Ok(()) }
//! ```

use std::{
    collections::HashMap,
};

use anyhow::Result;

use nettle::{
    random::*,
};

pub mod cv25519;

/// Message key.
pub type Key = [u8; 32];

fn generate_shared_secret() -> Key {
    let mut k = Key::default();
    let mut rng = Yarrow::default();
    rng.random(&mut k);
    k
}

/// Abstraction for the Diffie-Hellman key exchange.
pub trait DH {
    /// Secret parameter.
    type Secret: AsRef<[u8]>;
    /// Public parameter.
    type Public: AsRef<[u8]> + Clone + core::hash::Hash + PartialEq + Eq;
    /// Generates new parameters.
    fn generate() -> Result<(Self::Secret, Self::Public)>;
    /// Derives a shared secret.
    fn derive(secret: &Self::Secret, public: &Self::Public)
              -> Result<Self::Secret>;
}

/// Abstraction for the key derivation function used in the
/// Diffie-Hellman ratchet.
pub trait KdfRK {
    fn new(k: Key) -> Self;
    fn step(&mut self, key: &[u8]) -> Result<Key>;
}

/// Abstraction for the key derivation function used in the
/// symmetric-key ratchets.
pub trait KdfCK {
    fn new(k: Key) -> Self;
    fn step(&mut self) -> Result<Key>;
}

/// Diffie-Hellman parameters and shared secret.
pub struct PreKeyPair<D: DH> {
    dh_send: (D::Secret, D::Public),
    shared_secret: Key,
}

impl<D: DH> PreKeyPair<D> {
    /// Creates a new pre-key pair.
    pub fn new() -> Result<Self> {
        Ok(PreKeyPair {
            dh_send: D::generate()?,
            shared_secret: generate_shared_secret(),
        })
    }

    /// Derives the part of the pre-key pair to be communicated to the
    /// peer.
    pub fn pre_key(&self) -> PreKey<D> {
        PreKey {
            dh_send: self.dh_send.1.clone(),
            shared_secret: self.shared_secret.clone(),
        }
    }
}

/// The part of the pre-key pair to be communicated to the peer.
pub struct PreKey<D: DH> {
    dh_send: D::Public,
    shared_secret: Key,
}

/// A half-initialized Double Ratchet.
///
/// This can be used to encrypt messages to the peer.  The ratchet is
/// fully initialized when the first message is decrypted.
pub struct RatchetHalfOpen<D: DH, R: KdfRK, C: KdfCK> {
    dh_send: (D::Secret, D::Public),
    dh_receive: D::Public,
    rk: R,
    ck_s: C,
    n_s: usize,
}

impl<D: DH, R: KdfRK, C: KdfCK> RatchetHalfOpen<D, R, C> {
    /// Initializes the sending-part of a Double Ratchet.
    pub fn new(pk: PreKey<D>) -> Result<Self> {
        let (secret, public) = D::generate()?;
        let mut rk = R::new(pk.shared_secret);
        let ck_s = rk.step(D::derive(&secret, &pk.dh_send)?.as_ref())?;
        Ok(RatchetHalfOpen {
            dh_send: (secret, public),
            dh_receive: pk.dh_send,
            rk,
            ck_s: C::new(ck_s),
            n_s: 0,
        })
    }

    /// Derives a message key for encryption.
    pub fn encrypt(&mut self) -> Result<(Header<D>, Key)> {
        let mk = self.ck_s.step()?;
        let header = Header::new_half_open(self);
        self.n_s += 1;
        Ok((header, mk))
    }

    /// Derives a message key for decryption and finalizes the
    /// initialization of the Double Ratchet.
    pub fn decrypt(self, header: Header<D>) -> Result<(Ratchet<D, R, C>, Key)> {
        let mut r = Ratchet {
            half: self,
            // Initialized during DH rachet step:
            ck_r: C::new(Default::default()),
            n_r: 0,
            pn: 0,
            skipped_keys: Default::default(),
        };

        // Derive the message key.  The DH ratchet step will
        // initialize the chains.
        r.skip_message_keys(header.pn)?;
        r.dh_ratchet(&header)?;
        r.skip_message_keys(header.n_s)?;
        let mk = r.ck_r.step()?;
        r.n_r += 1;
        Ok((r, mk))
    }
}

/// A fully-initialized Double Ratchet.
pub struct Ratchet<D: DH, R: KdfRK, C: KdfCK> {
    half: RatchetHalfOpen<D, R, C>,
    ck_r: C,
    n_r: usize,
    pn: usize,
    skipped_keys: HashMap<(D::Public, usize), Key>,
}

impl<D: DH, R: KdfRK, C: KdfCK> Ratchet<D, R, C> {
    /// Initializes the Double Ratchet and derives a message key for
    /// decryption.
    pub fn new(
        pre_key: PreKeyPair<D>,
        header: Header<D>,
    ) -> Result<(Self, Key)> {
        let mut r = Ratchet {
            half: RatchetHalfOpen::<D, R, C> {
                dh_send: pre_key.dh_send,
                dh_receive: header.dh_send.clone(),
                // Initialized during DH rachet step:
                rk: R::new(pre_key.shared_secret),
                // Initialized during DH rachet step:
                ck_s: C::new(Default::default()),
                n_s: 0,
            },
            // Initialized during DH rachet step:
            ck_r: C::new(Default::default()),
            n_r: 0,
            pn: 0,
            skipped_keys: Default::default(),
        };

        // Derive the message key.  The DH ratchet step will
        // initialize the chains.
        r.skip_message_keys(header.pn)?;
        r.dh_ratchet(&header)?;
        r.skip_message_keys(header.n_s)?;
        let mk = r.ck_r.step()?;
        r.n_r += 1;
        Ok((r, mk))
    }

    /// Derives a message key for encryption.
    pub fn encrypt(&mut self) -> Result<(Header<D>, Key)> {
        let mk = self.half.ck_s.step()?;
        let header = Header::new(self);
        self.half.n_s += 1;
        Ok((header, mk))
    }

    /// Derives a message key for decryption.
    pub fn decrypt(&mut self, header: Header<D>) -> Result<Key> {
        let header_key = (header.dh_send.clone(), header.n_s);
        if let Some(mk) = self.skipped_keys.remove(&header_key) {
            return Ok(mk);
        }

        if header.dh_send != self.half.dh_receive {
            self.skip_message_keys(header.pn)?;
            self.dh_ratchet(&header)?;
        }

        self.skip_message_keys(header.n_s)?;
        let mk = self.ck_r.step()?;
        self.n_r += 1;
        Ok(mk)
    }

    /// Skips over message keys, saving them for out-of-order
    /// decryption.
    fn skip_message_keys(&mut self, until: usize) -> Result<()> {
        // XXX: Check MAX_SKIP to prevent DOS.
        while self.n_r < until {
            let mk = self.ck_r.step()?;
            self.skipped_keys.insert((self.half.dh_receive.clone(), self.n_r),
                                     mk);
            self.n_r += 1;
        }
        Ok(())
    }

    /// Performs a DH ratchet step.
    fn dh_ratchet(&mut self, h: &Header<D>) -> Result<()> {
        // Remember the number of sent messages using the previous DH
        // ratchet.
        self.pn = self.half.n_s;

        // Reset counters.
        self.half.n_s = 0;
        self.n_r = 0;

        // Update peer's DH parameter.
        self.half.dh_receive = h.dh_send.clone();

        // Re-initialize the receiving symmetric key ratchet.
        self.ck_r = C::new(self.half.rk.step(
            D::derive(&self.half.dh_send.0, &self.half.dh_receive)?.as_ref())?);

        // Generate new DH parameters.
        self.half.dh_send = D::generate()?;

        // Re-initialize the sending symmetric key ratchet.
        self.half.ck_s = C::new(self.half.rk.step(
            D::derive(&self.half.dh_send.0, &self.half.dh_receive)?.as_ref())?);

        Ok(())
    }
}

/// Communicates new Diffie-Hellman parameter and sequence numbers.
pub struct Header<D: DH> {
    dh_send: D::Public,
    pn: usize,
    n_s: usize,
}

impl<D: DH> Header<D> {
    fn new_half_open<R: KdfRK, C: KdfCK>(ratchet: &RatchetHalfOpen<D, R, C>) -> Self {
        Header {
            dh_send: ratchet.dh_send.1.clone(),
            pn: 0,
            n_s: ratchet.n_s,
        }
    }

    fn new<R: KdfRK, C: KdfCK>(ratchet: &Ratchet<D, R, C>) -> Self {
        Header {
            dh_send: ratchet.half.dh_send.1.clone(),
            pn: ratchet.pn,
            n_s: ratchet.half.n_s,
        }
    }
}

enum CKInput {
    MessageKey,
    ChainKey,
}

impl CKInput {
    fn as_bytes(&self) -> &'static [u8] {
        match self {
            CKInput::MessageKey => &[0x01],
            CKInput::ChainKey => &[0x02],
        }
    }
}

#[cfg(test)]
mod tests {
}
